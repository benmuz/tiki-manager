image: xorti/tiki-manager-qa:7.1-qa

cache:
  paths:
  - .composercache/files

variables:
  MYSQL_ROOT_PASSWORD: secret
  COMPOSER_CACHE_DIR: "$CI_PROJECT_DIR/.composercache"
  COMPOSER_FLAGS: '--ansi --no-progress --prefer-dist -n'

stages:
- quality
- unit-tests
- functional-tests
- package


#
# Lint
#

php-53-scripts:
  image: helder/php-5.3:latest
  stage: quality
  script:
    - php -l scripts/checkversion.php
    - php -l scripts/package_tar.php
    - php -l scripts/extract_tar.php
    - php -l scripts/get_extensions.php
    - php -l scripts/tiki/backup_database.php
    - php -l scripts/tiki/get_directory_list.php
    - php -l scripts/tiki/get_system_config_ini_file.php
    - php -l scripts/tiki/remote_install_profile.php
    - php -l scripts/tiki/sqlupgrade.php
    - php -l scripts/tiki/run_sql_file.php
    - php -l scripts/tiki/tiki_dbinstall_ftp.php
    - php -l scripts/tiki/remote_setup_channels.php
    - php -l scripts/tiki/mysqldump.php
  allow_failure: false

phpcs:
  stage: quality
  script:
    - composer install $COMPOSER_FLAGS
    - git log -m --first-parent -1 --name-only --diff-filter=d --pretty="format:" | grep -v "^$" | sort -u | grep '\.php$' - 2>&1 > /dev/null || { git log -m -1 --name-only && echo && echo 'No files to be processed. Skipping...' && echo && exit 0; }
    - git log -m -1 --name-only
    - git log -m --first-parent -1 --name-only --diff-filter=d --pretty="format:" | grep -v "^$" | sort -u | xargs php vendor/squizlabs/php_codesniffer/bin/phpcs -s --runtime-set ignore_warnings_on_exit true
  allow_failure: true
  only:
    - master

phpcs-branches:
  stage: quality
  variables:
    SBRANCH: "master"
  script:
    - composer install $COMPOSER_FLAGS
    - git diff --name-status origin/$SBRANCH | grep -v "^D" | cut -c 3- | sort -u | grep '\.php$' - 2>&1 > /dev/null || { git diff --name-status origin/$SBRANCH && echo && echo 'No files to be processed. Skipping...' && echo && exit 0; }
    - git diff --name-status origin/$SBRANCH
    - git diff --name-status origin/$SBRANCH | grep -v "^D" | cut -c 3- | xargs php vendor/squizlabs/php_codesniffer/bin/phpcs -s --runtime-set ignore_warnings_on_exit true
  allow_failure: true
  except:
    - master


code_quality:
  image: docker:stable
  stage: quality
  variables:
    DOCKER_DRIVER: overlay2
  allow_failure: true
  services:
    - docker:stable-dind
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
      --env SOURCE_CODE="$PWD"
      --volume "$PWD":/code
      --volume /var/run/docker.sock:/var/run/docker.sock
      "registry.gitlab.com/gitlab-org/security-products/codequality:$SP_VERSION" /code
  artifacts:
    reports:
      codequality: gl-code-quality-report.json

#----------------------------------------------------------------------------------------------------------------------#
# UNIT TESTS SECTION
#----------------------------------------------------------------------------------------------------------------------#
unit-tests:
  stage: unit-tests
  services:
    - name: mysql:5.6
      alias: mysql
  script:
    - echo 'APP_ENV=gitlab-ci' >> .env
    - ssh-keygen -t rsa -f ./data/id_rsa -q -P ""
    - composer install $COMPOSER_FLAGS
    - ./vendor/bin/phpunit --bootstrap src/env_setup.php tests/ --group unit
  allow_failure: false


#----------------------------------------------------------------------------------------------------------------------#
# FUNCTIONAL TESTS SECTION
#----------------------------------------------------------------------------------------------------------------------#

.template-tiki-manager-commands: &template-tiki-manager-commands
  stage: functional-tests
  services:
    - name: mysql:5.6
      alias: mysql
  script:
    - echo APP_ENV=gitlab-ci >> .env
    - echo DEFAULT_VCS=$VCS >> .env
    - ssh-keygen -t rsa -f ./data/id_rsa -q -P ""
    - composer install $COMPOSER_FLAGS
    - composer bin all install $COMPOSER_FLAGS
    - ./vendor/bin/phpunit --bootstrap src/env_setup.php tests/Command/$TEST_FILE
  allow_failure: false

.template-tiki-manager-commands-git: &template-tiki-manager-commands-git
  <<: *template-tiki-manager-commands
  variables:
    VCS: git

.template-tiki-manager-commands-svn: &template-tiki-manager-commands-svn
  <<: *template-tiki-manager-commands
  variables:
    VCS: svn

create-instance-git:
  <<: *template-tiki-manager-commands-git
  variables:
    TEST_FILE: CreateInstanceCommandTest.php

create-instance-svn:
  <<: *template-tiki-manager-commands-svn
  variables:
    TEST_FILE: CreateInstanceCommandTest.php

backup-instance-git:
  <<: *template-tiki-manager-commands-git
  variables:
    TEST_FILE: BackupInstanceCommandTest.php

backup-instance-svn:
  <<: *template-tiki-manager-commands-svn
  variables:
    TEST_FILE: BackupInstanceCommandTest.php

restore-instance-git:
  <<: *template-tiki-manager-commands-git
  variables:
    TEST_FILE: RestoreInstanceCommandTest.php

restore-instance-svn:
  <<: *template-tiki-manager-commands-svn
  variables:
    TEST_FILE: RestoreInstanceCommandTest.php

clone-instance-git:
  <<: *template-tiki-manager-commands-git
  variables:
    TEST_FILE: CloneInstanceCommandTest.php

clone-instance-svn:
  <<: *template-tiki-manager-commands-svn
  variables:
    TEST_FILE: CloneInstanceCommandTest.php

clone-upgrade-instance-git:
  <<: *template-tiki-manager-commands-git
  variables:
    TEST_FILE: CloneAndUpgradeCommandTest.php

clone-upgrade-instance-svn:
  <<: *template-tiki-manager-commands-svn
  variables:
    TEST_FILE: CloneAndUpgradeCommandTest.php

#----------------------------------------------------------------------------------------------------------------------#
# PACKAGE SECTION
#----------------------------------------------------------------------------------------------------------------------#

phar:
  stage: package
  script:
    - composer install $COMPOSER_FLAGS
    - composer build-phar
  allow_failure: false
  artifacts:
    name: tiki-manager.phar
    paths:
      - build/tiki-manager.phar
    when: on_success
  only:
    refs:
      - master
      - tags
